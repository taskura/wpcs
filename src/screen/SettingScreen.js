

import React, { Component } from 'react';
import { ScrollView,View,Text,StyleSheet,StatusBar,TextInput,Dimensions,Image } from 'react-native';
import { Button } from 'native-base';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StackedBarChart } from "react-native-chart-kit";
import axios from "axios";

class SettingScreen extends Component {

  constructor(props) {
    super(props);


  this.state = {
    threshvalue1: '',
    threshvalue2: '',
  }

  this.handleChangeText = this.handleChangeText.bind(this)
  }

  handleChangeText(){

    axios.post('https://taskura.pythonanywhere.com/update/threshold_value/api/', {
    threshvalue1: this.state.threshvalue1,
    threshvalue2: this.state.threshvalue2
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
  }


    componentDidMount() {
      axios.get(`https://taskura.pythonanywhere.com/threshold/api/`)
        .then(res => {
          const threshvalue1 = res.data.t1;
          const threshvalue2 = res.data.t2;

          this.setState({ threshvalue1 });
          this.setState({ threshvalue2 });



        })
    }




  render(){

    return(
      <View style={styles.container}>

           <StatusBar backgroundColor="#297d4e" barStyle="light-content" />

           <View style={styles.header}>
           <Icon
              style={styles.iconStyle}
              name="bars"
              size={30}
              onPress = {() => this.props.navigation.openDrawer()}
              />
                <Text style={styles.headerTiTle}>WPCS</Text>
           </View>


           <View style={styles.settingbody}>
               <View style={styles.inputBox}>
                   <Text style={styles.buttontext}>Threshold value for North</Text>
                   <TextInput placeholder = 'Threshold value for North'
                             onChangeText={(threshvalue1) => this.setState({threshvalue1})}
                             value = {this.state.threshvalue1}
                             style = {{ backgroundColor:'#ebeef2',borderColor:'black',
                                 borderWidth:1, }}
                   />
                   <Text style={styles.buttontext}>Threshold value for South</Text>
                   <TextInput placeholder = 'Threshold value for South'
                              onChangeText={(threshvalue2) => this.setState({threshvalue2})}
                              value = {this.state.threshvalue2}
                              style = {{ backgroundColor:'#ebeef2',borderColor:'black',
                           borderWidth:1, }}
                   />
               </View>
               <Button style={styles.button2Style} onPress={ this.handleChangeText }>
               <Text style={styles.buttontext}> Update </Text>
               </Button>

               </View>


            </View>

      );
  }
}

export default SettingScreen

const styles = StyleSheet.create({
  container:{

  flex:1
  },
  header:{
    height:'12%',
    backgroundColor:'#297d4e',

  },
  settingbody:{
    height: '88%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2e8b57'
  },


  textContent:{
   color:'#fff',
   fontSize:15,
   marginTop:10,
   marginLeft:50

},
iconStyle:{
     paddingLeft:15,
     paddingTop:20,
     color:'#fff',
     height:50,

   },
headerTiTle:{
     fontSize:25,
     marginLeft:150,
     color:'#fff',

   },

buttontext:{
     color:'#fff',
     fontSize:15,
     },

inputBox:{

       width:310,

       },
button2Style:{
      height:40,
      width:310,
      marginTop:50,
      backgroundColor:'#018a3d',
      alignItems: 'center',
      justifyContent: 'center',

     },



     })
