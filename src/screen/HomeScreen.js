import React, { Component } from 'react';
import { ScrollView,View,Text,StyleSheet,StatusBar,TextInput,Dimensions,Image } from 'react-native';
import { Button } from 'native-base';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StackedBarChart } from "react-native-chart-kit";
import axios from "axios";







class HomeScreen extends Component {

  state = {
    sensor1: '',
    sensor2: '',
    sensor3: '',
    sensor4: ''
  }


  componentDidMount() {
  setInterval(() => (
    axios.get(`https://taskura.pythonanywhere.com/sensor_valus/api/`)
      .then(res => {
        const sensor1 = res.data.sensor1;
        const sensor2 = res.data.sensor2;
        const sensor3 = res.data.sensor3;
        const sensor4 = res.data.sensor4;
        this.setState({ sensor1 });
        this.setState({ sensor2 });
        this.setState({ sensor3 });
        this.setState({ sensor4 });


      })
  ), 5000);
  }








  render(){
    return(
      <View style={styles.container}>
               <StatusBar backgroundColor="#297d4e" barStyle="light-content" />
               <View style={styles.header}>
               <Icon
                  style={styles.iconStyle}
                  name="bars"
                  size={30}
                  onPress = {() => this.props.navigation.openDrawer()}
                  />
                    <Text style={styles.headerTiTle}>WPCS</Text>
               </View>
               <View style={styles.homepage}>
                <View style={styles.body}>
                <View style= {styles.bodyItem}>
                <View style= {styles.bodyItemInner}>
                <Text style={styles.sensore}>{this.state.sensor1}%</Text>
                </View>
                    <Text style={styles.textContent}>Sensor One</Text>
                </View>

                <View style= {styles.bodyItem}>
                <View style= {styles.bodyItemInner}>
                  <Text style={styles.sensore}>{this.state.sensor3}%</Text>
                </View>
                  <Text style={styles.textContent}>Sensor Three</Text>
                </View>
                <View style= {styles.bodyItem}>
                <View style= {styles.bodyItemInner}>
                  <Text style={styles.sensore}>{this.state.sensor2}%</Text>
                  </View>
                  <Text style={styles.textContent}>Sensor Two</Text>
                </View>
                <View style= {styles.bodyItem}>
                <View style= {styles.bodyItemInner}>
                  <Text style={styles.sensore}>{this.state.sensor4}%</Text>
                </View>
                  <Text style={styles.textContent}>Sensor Four</Text>
                </View>

                </View>

               </View>

           </View>

      );
  }
}

 export default HomeScreen

 const styles = StyleSheet.create({

    container:{

    flex:1
    },
    header:{
      height:'12%',
      backgroundColor:'#297d4e',

    },
    homepage:{
      height: '88%',
      width: '100%',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#2e8b57'
    },

    body:{
     height:'100%',
     width:'100%',
     backgroundColor:'#2e8b57',
     flexDirection: 'row',
     flexWrap:'wrap',
     padding:5,
    },
    bodyItem:{
      width:'50%',
      height:'40%',
      padding:10,
      marginTop:20,


    },
    bodyItemInner:{
      flex:1,
      backgroundColor:'#3cb371',
      alignItems:'center',
      justifyContent: 'center',
       borderRadius:20,

    },
    sensore:{
      color:'#fff',
       fontSize:40,

    },
    textContent:{
     color:'#fff',
     fontSize:15,
     marginTop:10,
     marginLeft:50

 },
 iconStyle:{
       paddingLeft:15,
       paddingTop:20,
       color:'#fff',
       height:50,

     },
 headerTiTle:{
       fontSize:25,
       marginLeft:150,
       color:'#fff',

     },


 });
