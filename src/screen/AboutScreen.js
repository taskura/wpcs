
import React, { Component } from 'react';
import { ScrollView,View,Text,StyleSheet,StatusBar,TextInput,Dimensions,Image } from 'react-native';
import { Button } from 'native-base';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StackedBarChart } from "react-native-chart-kit";
import axios from "axios";

class AboutScreen extends Component {
  render(){
    return(
      <ScrollView >
      <View style={styles.header}>
      <Icon
      style={styles.iconStyle}
      name="bars"
      size={30}
      onPress = {() => this.props.navigation.openDrawer()}
      />
        <Text style={styles.headerTiTle}>WPCS</Text>
      </View>
      <View style={{ padding:15 }}>
        <View  style={styles.summaryContent}>

          <Text style={{fontSize:25}}>About</Text>

              <Text style={{textAlign: "center",textAlign: "justify"}}>
              Main objective for our research that we can maintain the soil moisture status and based on that
              we can maintain water level.Thats how we can save our water.
              </Text>

        </View>

        <View  style={styles.supervisor}>
        <Image
            style={{width: 100, height: 100}}
            source={require('../../assets/Rashedul-Islam.jpg')}
            />
            <Text>Md.Rashedul Islam</Text>
            <Text>Project Supervisor</Text>
            <Text>Senior Lecturer</Text>
            <Text>Department of Computer Science and Engineering</Text>
            <Text>IUBAT</Text>
        </View>
        <View style={styles.developer}>
        <View style={styles.dev1}>
            <Image
              style={{width: 100, height: 100}}
              source={require('../../assets/Shaid-Hasan.jpg')}
              />
              <Text>Shaid Hasan Shawon</Text>
              <Text>Project Developer</Text>
              <Text>Student</Text>
              <Text>Department of Computer Science and Engineering</Text>
              <Text>IUBAT</Text>



          </View>
          <View style={styles.dev2}>
            <Image
                style={{width: 100, height: 100}}
                source={require('../../assets/taskura.jpg')}
                />
                <Text>Taskujratul Qubra</Text>
                <Text>Project Developer</Text>
                <Text>Student</Text>
                <Text>Department of Computer Science and Engineering</Text>
                <Text>IUBAT</Text>

            </View>

        </View>

      </View>
      </ScrollView>
      );
  }
}

export default AboutScreen

const styles = StyleSheet.create({
  header:{
    flexDirection:'row',
    backgroundColor:'#297d4e',
    height:55,


    },
   iconStyle:{
     paddingLeft:10,
     paddingTop:13,
     color:'#fff',
     height:50,
     flex:1
   },
   headerTiTle:{
     flex:2,
     fontSize:25,
     marginTop:10,
     marginLeft:20,
     color:'#fff'
   },
   scrollcontent:{
     backgroundColor:'#2e8b57',


   },
   blockContent:{
     flex:1,
     flexDirection:'row',
     alignItems:'center',
     justifyContent: 'center',


   },
   block1:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:20,
     marginTop:70,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',



   },
   block2:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:20,
     marginTop:60,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',

   },
   block3:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:10,
     marginTop:70,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',

   },
   block4:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:10,
     marginTop:60,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',

   },
   textContent1:{
     color:'#fff',
     fontSize:40,
     marginLeft:20

   },
   textContent2:{
     color:'#fff',
     fontSize:15,
     marginLeft:60
   },
   buttontext:{
     color:'#fff',
     fontSize:15,
     },
   botton:{
       flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:100

   },
   settingText:{
     fontSize:18
   },
   button1Style:{
    height:50,
    width:310,
    backgroundColor:'#00a146',
    alignItems: 'center',
    justifyContent: 'center',

   },
   inputBox:{
     marginTop:100,
     width:310,
     height:100
     },
   button2Style:{
    height:40,
    width:310,
    marginTop:50,
    backgroundColor:'#018a3d',
    alignItems: 'center',
    justifyContent: 'center',

   },

Viewcontent:{
 flexDirection:'column',

 },
 textView:{
   flex:1,
   marginTop:20,
   fontSize:20,
 },
 graphview:{
   flex:2,
   marginTop:25,
   alignItems: 'center',
   justifyContent: 'center',
 },

   summaryContent:{
         marginTop:10,
         flex:1,
         alignItems:'center',
         justifyContent:'center'
   },

   supervisor:{
     marginTop:25,
     flex:1,
     alignItems:'center',
     justifyContent:'center'
   },
   developer:{
     flexDirection:'row',
     marginTop:20
   },
   dev1:{
       flex:2,
       height:300,
       alignItems:'center',

   },
   dev2:{
     flex:2,
     height:300,
     alignItems:'center',
   }



     })
