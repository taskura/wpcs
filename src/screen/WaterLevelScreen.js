import React, { Component } from 'react';
import { ScrollView,View,Text,StyleSheet,StatusBar,TextInput,Dimensions,Image } from 'react-native';
import { Button } from 'native-base';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Col, Row, Grid } from "react-native-easy-grid";
import { StackedBarChart } from "react-native-chart-kit";
import axios from "axios";


class WaterLevelScreen extends Component {
  constructor(props) {
    super(props);


  this.state = {
    data1: {

        labels: [],
        legend: ['Have water', 'Need water'],
        data: [],
        barColors: ['#297d4e', '#fc0313'],

    },

    data2: {

        labels: [],
        legend: ['Have water', 'Need water'],
        data: [],
        barColors: ['#297d4e', '#fc0313'],

    },

  };

  }

  componentDidMount() {

    setInterval(() => (

      axios.get(`https://taskura.pythonanywhere.com/soilmoisture/api/`)
        .then(res => {
          const chart1data = res.data.chart1.data;
          const chart1levels = res.data.chart1.labels;
           const dataClone = {...this.state.data1}
           dataClone.data = chart1data;
           dataClone.labels = chart1levels;

           const chart2data = res.data.chart2.data;
           const chart2levels = res.data.chart2.labels;
           const data2Clone = {...this.state.data2}
           data2Clone.data = chart2data;
           data2Clone.labels = chart2levels;

          this.setState(
            {
              data1: dataClone,
              data2: data2Clone

               },
            );
        })

        ), 5000);


  }

  render(){
    return(
         <ScrollView style={styles.Viewcontent}>
            <View style={styles.header}>
              <Icon
              style={styles.iconStyle}
              name="bars"
              size={30}
              onPress = {() => this.props.navigation.openDrawer()}
              />
              <Text style={styles.headerTiTle}>WPCS</Text>
            </View>
            <View style={{marginTop:10,flex:1,alignItems:'center',justifyContent:'center'}}>
              <Text style={{fontSize:25}}>Water level</Text>
            </View>
            <View>
            <Text style={styles.textView}>  Required water level for NorthSide </Text>
            </View>
            <View style={styles.graphview}>
                 <StackedBarChart
                   data={this.state.data1}
                   width={Dimensions.get('window').width - 16}
                   height={220}
                   chartConfig={{
                     backgroundColor: '#1cc910',
                     backgroundGradientFrom: '#eff3ff',
                     backgroundGradientTo: '#efefef',
                     decimalPlaces: 2,
                     color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                     style: {
                       borderRadius: 16,
                     },
                   }}
                   style={{
                     marginLeft:10,
                     borderRadius: 2,
                   }}
             />
             </View>
             <View>
             <Text style={styles.textView}>  Required water level for SouthSide </Text>
             </View>
             <View style={styles.graphview}>
                  <StackedBarChart
                    data={this.state.data2}
                    width={Dimensions.get('window').width - 16}
                    height={220}
                    chartConfig={{
                      backgroundColor: '#1cc910',
                      backgroundGradientFrom: '#eff3ff',
                      backgroundGradientTo: '#efefef',
                      decimalPlaces: 2,
                      color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                      style: {
                        borderRadius: 16,
                      },
                    }}
                    style={{
                      marginLeft:10,
                      borderRadius: 2,
                    }}
              />
              </View>

         </ScrollView>
      );
  }
}
export default WaterLevelScreen


const styles = StyleSheet.create({
  header:{
    flexDirection:'row',
    backgroundColor:'#297d4e',
    height:55,


    },
   iconStyle:{
     paddingLeft:10,
     paddingTop:13,
     color:'#fff',
     height:50,
     flex:1
   },
   headerTiTle:{
     flex:2,
     fontSize:25,
     marginTop:10,
     marginLeft:20,
     color:'#fff'
   },
   scrollcontent:{
     backgroundColor:'#2e8b57',


   },
   blockContent:{
     flex:1,
     flexDirection:'row',
     alignItems:'center',
     justifyContent: 'center',


   },
   block1:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:20,
     marginTop:70,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',



   },
   block2:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:20,
     marginTop:60,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',

   },
   block3:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:10,
     marginTop:70,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',

   },
   block4:{
     backgroundColor:'#3cb371',
     height:200,
     width:180,
     borderRadius:20,
     alignItems:'center',
     justifyContent:'center',
     marginRight:40,
     marginLeft:10,
     marginTop:60,
     flex:1,
     alignItems:'center',
     justifyContent: 'center',

   },
   textContent1:{
     color:'#fff',
     fontSize:40,
     marginLeft:20

   },
   textContent2:{
     color:'#fff',
     fontSize:15,
     marginLeft:60
   },
   buttontext:{
     color:'#fff',
     fontSize:15,
     },
   botton:{
       flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop:100

   },
   settingText:{
     fontSize:18
   },
   button1Style:{
    height:50,
    width:310,
    backgroundColor:'#00a146',
    alignItems: 'center',
    justifyContent: 'center',

   },
   inputBox:{
     marginTop:100,
     width:310,
     height:100
     },
   button2Style:{
    height:40,
    width:310,
    marginTop:50,
    backgroundColor:'#018a3d',
    alignItems: 'center',
    justifyContent: 'center',

   },

Viewcontent:{
 flexDirection:'column',

 },
 textView:{
   flex:1,
   marginTop:20,
   fontSize:20,
 },
 graphview:{
   flex:2,
   marginTop:25,
   alignItems: 'center',
   justifyContent: 'center',
 },

   summaryContent:{
         marginTop:10,
         flex:1,
         alignItems:'center',
         justifyContent:'center'
   },

   supervisor:{
     marginTop:25,
     flex:1,
     alignItems:'center',
     justifyContent:'center'
   },
   developer:{
     flexDirection:'row',
     marginTop:20
   },
   dev1:{
       flex:2,
       height:300,
       alignItems:'center',

   },
   dev2:{
     flex:2,
     height:300,
     alignItems:'center',
   }



     })
